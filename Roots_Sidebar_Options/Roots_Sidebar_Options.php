<?php 
/*
 * Plugin Name: 	Roots Sidebar Options
 * Description: 	Customizes a Roots sidebar based theme with some options
 * Plugin URI: https://bitbucket.org/gd97/roots-sage-plugins
 * Version: 0.2
 * Author: Daniel Gandolfi
 * Author URI: https://bitbucket.org/gd97/
 */
// check if class is instatiated or instantiate it 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if (!isset(Roots_Sidebar_Options::$_instance))
	new Roots_Sidebar_Options();

class Roots_Sidebar_Options
{
    public static $slug = 'Roots_Sidebar_Options';
	public static $_instance =null;
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		$_instance = $this;
		// hook to meta box creator
		add_action( 
			'add_meta_boxes', 
			array( $this, 'add_meta_box' ) 
		);
		// hook to save action
		add_action( 
			'save_post', 
			array( $this, 'save' ) 
		);
		// add a filter to the sage display sidebar option
		add_filter( 
			'roots/display_sidebar', 
			array( $this, 'roots_sidebar_display_filter' ), 
			0 
		);
		// add a filter to the sage css class sidebar option
		add_filter( 
			'roots/sidebar_class', 
			array( $this, 'roots_sidebar_classes_filter'), 
			0
		);
	}
	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
		$post_types = apply_filters( Roots_Sidebar_Options::$slug . '_post_types',
			get_post_types(
				array(
					'public'  => true,
					'show_ui' => true
				)
			)
		);
		if ( in_array( $post_type, $post_types ) ) {
			add_meta_box(
				Roots_Sidebar_Options::$slug
				, __( 'Roots sidebar attributes', Roots_Sidebar_Options::$slug )
				, array( $this, 'render_meta_box_content' )
				, $post_type
				, 'side'
				, 'default'
			);
		}
	}

	
	
	private function save_display($post_id)
	{

		/* OK, its safe for us to save the data now. */
		
		// Sanitize the user input.
		if ( ! isset( $_POST[ Roots_Sidebar_Options::$slug.'_display' ] ) ) {
			delete_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_display' );
		
			return $post_id;
		}
		
		if ( empty( $_POST[ Roots_Sidebar_Options::$slug.'_display' ] ) ) {
			delete_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_display' );
		
			return $post_id;
		}
		
		$display = sanitize_text_field( $_POST[ Roots_Sidebar_Options::$slug.'_display' ] );
		
		// Update the meta field.
		update_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_display', $display );
	}
	private function save_classes($post_id)
	{
	
		/* OK, its safe for us to save the data now. */
	
		// Sanitize the user input.
		if ( ! isset( $_POST[ Roots_Sidebar_Options::$slug.'_classes' ] ) ) {
			delete_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_classes' );
	
			return $post_id;
		}
	
		if ( empty( $_POST[ Roots_Sidebar_Options::$slug.'_classes' ] ) ) {
			delete_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_classes' );
	
			return $post_id;
		}
	
		$display = sanitize_text_field( $_POST[ Roots_Sidebar_Options::$slug.'_classes' ] );
	
		// Update the meta field.
		update_post_meta( $post_id, '_' . Roots_Sidebar_Options::$slug.'_classes', $display );
	}
	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public  function save( $post_id ) {

		// Check if our nonce is set.
		if ( ! isset( $_POST[ Roots_Sidebar_Options::$slug . '_nonce' ] ) ) {
			return $post_id;
		}

		$nonce = $_POST[ Roots_Sidebar_Options::$slug . '_nonce' ];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, Roots_Sidebar_Options::$slug ) ) {
			return $post_id;
		}

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}
		$this->save_display($post_id);
		$this->save_classes($post_id);
		
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( Roots_Sidebar_Options::$slug, Roots_Sidebar_Options::$slug . '_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$display = get_post_meta( $post->ID, '_' . Roots_Sidebar_Options::$slug.'_display', true );
		// show display sidebar option
		echo '<p><strong>'.__('Show sidebar', Roots_Sidebar_Options::$slug).'</strong></p>';
		echo '<label for="'.Roots_Sidebar_Options::$slug.'_display" class="screen-reader-text">'.__('Show sidebar', Roots_Sidebar_Options::$slug).'</label>';
		echo '<select id="'.Roots_Sidebar_Options::$slug.'_display" name="' . Roots_Sidebar_Options::$slug . '_display">';
		echo '<option value="">' . __( 'default', Roots_Sidebar_Options::$slug ) . '</option>';
		echo '<option value="show" ' . selected( $display, 'show', false ) . '>' . __( 'Show sidebar', Roots_Sidebar_Options::$slug ) . '</option>';
		echo '<option value="hide" ' . selected( $display, 'hide', false ) . '>' . __( 'Hide sidebar', Roots_Sidebar_Options::$slug ) . '</option>';
		echo '</select>';
		// show sidebar classes field
		$classes = get_post_meta( $post->ID, '_' . Roots_Sidebar_Options::$slug.'_classes', true ) 	;
		echo "<p><strong>".__('Sidebar css classes', Roots_Sidebar_Options::$slug)."</strong></p>";
		echo '<label for="'.Roots_Sidebar_Options::$slug.'_classes" class="screen-reader-text">'.__('Sidebar css classes', Roots_Sidebar_Options::$slug).'</label>';
		echo '<input type="text" id="'.Roots_Sidebar_Options::$slug.'_classes" name="'.Roots_Sidebar_Options::$slug.'_classes" value="'.$classes.'"	>';
		
	}


	function roots_sidebar_display_filter( $sidebar ) {
		global $post;
		$display = get_post_meta( $post->ID, '_' . Roots_Sidebar_Options::$slug.'_display', true );
		switch ( $display ) 
		{
			case 'show':
				return true;
			case 'hide':
				return false;
			default:
				return $sidebar;
		}

		return $sidebar;
	}
	function roots_sidebar_classes_filter( $classes) {
		global $post;
		$meta = ' '.get_post_meta( $post->ID, '_' . Roots_Sidebar_Options::$slug.'_classes', true ).' ';
		if (!empty(trim($meta)))
			$classes =$meta;
		
		return $classes;
	}
}


?>