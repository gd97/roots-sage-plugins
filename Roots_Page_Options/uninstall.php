<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
require_once dirname(__FILE__).'/Roots_Page_Options.php';
global $wpdb;
$query ="DELETE FROM $wpdb->postmeta ".
    ' WHERE `meta_key` LIKE "'.'_'.Roots_Page_Options::$slug."_classes_with_sidebar".'"'. 
    ' OR `meta_key` LIKE "'. '_'.Roots_Page_Options::$slug."_classes_without_sidebar".'"';
$wpdb->show_errors();
$wpdb->query($query, OBJECT);

?>