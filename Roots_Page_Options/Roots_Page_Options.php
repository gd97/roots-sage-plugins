<?php
/*
 * Plugin Name: Roots Page Options
 * Description: Customizes a Roots page based theme with some options
 * Plugin URI: https://bitbucket.org/gd97/roots-sage-plugins
 * Version: 0.2
 * Author: Daniel Gandolfi
 * Author URI: https://bitbucket.org/gd97/
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// check if class is instatiated or instantiate it 
if (! isset ( Roots_Page_Options::$_instance ))
	Roots_Page_Options::$_instance = new Roots_Page_Options();

class Roots_Page_Options {
	public static $slug = 'Roots_Page_Options';
	public static $_instance = null;
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		// hook to meta box creator
		add_action ( 
			'add_meta_boxes', 
			array (
				$this,
				'add_meta_box' 
			) 
		);
		// hook to save post action
		add_action ( 
			'save_post', 
			array (
				$this,
				'save' 
			) 
		);
		// add a filter to the sage main class
		add_filter ( 
			'roots/main_class', 
			array (
				$this,
				'roots_main_classes_filter' 
			), 
			0 
		);
	}
	
	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box($post_type) {
		$post_types = apply_filters ( Roots_Page_Options::$slug . '_post_types', get_post_types ( array (
				'public' => true,
				'show_ui' => true 
		) ) );
		if (in_array ( $post_type, $post_types )) {
			add_meta_box ( 
				Roots_Page_Options::$slug, 
				__ ( 'Roots body attributes', Roots_Page_Options::$slug ), 
				array ($this, 'render_meta_box_content'), 
				$post_type, 
				'side', 
				'default' 
			);
		}
	}
	private function save_classes($post_id) {
		
		// if empty remove meta
		if (!isset ( $_POST [Roots_Page_Options::$slug . '_classes_with_sidebar'] )
				|| empty ( $_POST [Roots_Page_Options::$slug . '_classes_with_sidebar'] )) 
		{
			delete_post_meta ( $post_id, '_' . Roots_Page_Options::$slug . '_classes_with_sidebar' );
		}else
		{
			$display = sanitize_text_field ( $_POST [Roots_Page_Options::$slug . '_classes_with_sidebar'] );
			// Update the meta field.
			update_post_meta ( $post_id, '_' . Roots_Page_Options::$slug . '_classes_with_sidebar', $display );
		}
		
		
		// Sanitize the user input.
		// Update the without sidebar classes
		if (!isset ( $_POST [Roots_Page_Options::$slug . '_classes_without_sidebar'] )
				|| empty ( $_POST [Roots_Page_Options::$slug . '_classes_without_sidebar'] )) 
		{
			delete_post_meta ( $post_id, '_' . Roots_Page_Options::$slug . '_classes_without_sidebar' );
		}else 
		{
			$display = sanitize_text_field ( $_POST [Roots_Page_Options::$slug . '_classes_without_sidebar'] );
			update_post_meta ( $post_id, '_' . Roots_Page_Options::$slug . '_classes_without_sidebar', $display );
		}
	}
	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id
	 *        	The ID of the post being saved.
	 */
	public function save($post_id) {
		// Check if our nonce is set.
		if (! isset ( $_POST [Roots_Page_Options::$slug . '_nonce'] )) 
		{
			return $post_id;
		}
		
		$nonce = $_POST [Roots_Page_Options::$slug . '_nonce'];
		
		// Verify that the nonce is valid.
		if (! wp_verify_nonce ( $nonce, Roots_Page_Options::$slug )) 
		{
			return $post_id;
		}
		
		
		// Check the user's permissions.
		if ('page' == $_POST ['post_type']) {
			
			if (! current_user_can ( 'edit_page', $post_id )) 
			{
				return $post_id;
			}
		} else {
			
			if (! current_user_can ( 'edit_post', $post_id )) 
			{
				return $post_id;
			}
		}
		$this->save_classes ( $post_id );
	}
	
	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post
	 *        	The post object.
	 */
	public function render_meta_box_content($post) {
		
		// Add an nonce field so we can check for it later.
		wp_nonce_field ( Roots_Page_Options::$slug, Roots_Page_Options::$slug . '_nonce' );
		
		// Use get_post_meta to retrieve the css classes when sidebar is in use
		$classes = get_post_meta ( $post->ID, '_' . Roots_Page_Options::$slug . '_classes_with_sidebar', true ) ;
		echo '<p><strong>' . __ ( 'Body css classes with sidebar', Roots_Page_Options::$slug ) . '</strong></p>';
		echo '<label class="screen-reader-text" for="' . Roots_Page_Options::$slug . '_classes">' . __ ( 'Body css classes with sidebar', Roots_Page_Options::$slug ) . '</label>';
		echo '<input type="text" id="' . Roots_Page_Options::$slug . '_classes_with_sidebar" name="' . Roots_Page_Options::$slug . '_classes_with_sidebar" value="' . $classes . '">';
		// Use get_post_meta to retrieve the css classes when sidebar is NOT in use
		$classes = get_post_meta ( $post->ID, '_' . Roots_Page_Options::$slug . '_classes_without_sidebar', true ) ;
		echo '<p><strong>' . __ ( 'Body css classes without sidebar', Roots_Page_Options::$slug ) . '</strong></p>';
		echo '<label class="screen-reader-text" for="' . Roots_Page_Options::$slug . '_classes_without_sidebar">' . __ ( 'Body css classes with sidebar', Roots_Page_Options::$slug ) . '</label>';
		echo '<input type="text" id="' . Roots_Page_Options::$slug . '_classes_without_sidebar" name="' . Roots_Page_Options::$slug . '_classes_without_sidebar" value="' . $classes . '">';
	}
	function roots_main_classes_filter($classes) {
		global $post;
		$meta = null;
		//check if sidebar is active or not
		// and get the right classes 
		if (roots_display_sidebar ())
			$meta = ' ' . get_post_meta ( $post->ID, '_' . Roots_Page_Options::$slug . '_classes_with_sidebar', true ) . ' ';
		else
			$meta = ' ' . get_post_meta ( $post->ID, '_' . Roots_Page_Options::$slug . '_classes_without_sidebar', true ) . ' ';

		if (!empty(trim($meta)))
			$classes = $meta;
		return $classes;
	}
}

?>